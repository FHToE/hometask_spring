package com.bsa.spring.hometask.Cache.Dto;

import lombok.Data;

@Data
public class GenerateGifRequestDto {
    private String query;
}
