package com.bsa.spring.hometask.Cache;


import com.bsa.spring.hometask.Cache.Dto.CacheByQueryResponseDto;
import com.bsa.spring.hometask.Cache.Dto.GenerateGifRequestDto;
import com.bsa.spring.hometask.Exceptions.GifNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cache")
public class CacheController {
    @Autowired
    private CacheService cacheService;

    @GetMapping
    public List<CacheByQueryResponseDto> getCache(@RequestParam(required = false) String query) {
        List<CacheByQueryResponseDto> result = cacheService.getAllCache();
        if (query != null) result.removeIf(x -> !x.getQuery().equals(query));
        return result;
    }

    @PostMapping("/generate")
    public CacheByQueryResponseDto generateGif (@RequestBody GenerateGifRequestDto requestDto) {
        CacheByQueryResponseDto result = cacheService.generateAndSaveGif(requestDto.getQuery());
        if (result==null) throw new GifNotFoundException();
        return result;
    }

    @DeleteMapping
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteCache() {
        cacheService.deleteCache();
    }
}
