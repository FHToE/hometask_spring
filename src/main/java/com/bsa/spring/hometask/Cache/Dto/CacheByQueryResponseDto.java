package com.bsa.spring.hometask.Cache.Dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CacheByQueryResponseDto {
    private String query;
    private Set<String> gifs;
}
