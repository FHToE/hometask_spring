package com.bsa.spring.hometask.Cache;

import com.bsa.spring.hometask.Cache.Dto.CacheByQueryResponseDto;
import com.bsa.spring.hometask.Image.ImageService;
import com.bsa.spring.hometask.Configuration.Settings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.*;

@Service
public class CacheService {
    @Autowired
    private Settings settings;
    @Autowired
    private ImageService imageService;

    public List<CacheByQueryResponseDto> getAllCache() {
        List<CacheByQueryResponseDto> list = new ArrayList<>();
        File folder = new File(settings.getPROJECT_FOLDER() +
                File.separator + "cache");
        for (File fold : folder.listFiles()) {
            if (fold.isDirectory()) {
                Set<String> gifs = new HashSet<>();
                for (File gif : fold.listFiles()) {
                    if (gif.isFile()) gifs.add(gif.toString());
                }
                list.add(new CacheByQueryResponseDto(fold.getName(), gifs));
            }
        }
        return list;
    }

    public CacheByQueryResponseDto generateAndSaveGif(String query) {
        List<String> idList = imageService.getImageId(query);
        if (idList.size()==0) return null;
        File folder = new File(settings.getPROJECT_FOLDER() +
                File.separator + "cache" + File.separator + query);
        if (!folder.exists()) folder.mkdir();
        String id = idList.remove((int) (Math.random() * idList.size()));
        while  (Arrays.asList(folder.list()).contains(id + ".gif")) {
            if (idList.size()==0) {
                folder.delete();
                return null;
            }
            id = idList.remove((int) (Math.random() * idList.size()));
        }
        String destinationFile = folder + File.separator + id + ".gif";
        imageService.saveImage(id, destinationFile);
        Set<String> gifs = new HashSet<>();
        for (File gif : folder.listFiles()) {
            if (gif.isFile()) gifs.add(gif.toString());
        }
        return new CacheByQueryResponseDto(query, gifs);
    }

    public void deleteCache() {
        File folder = new File(settings.getPROJECT_FOLDER() +
                File.separator + "cache");
        recursiveDeleteFolder(folder);
        new File(settings.getPROJECT_FOLDER() +
                File.separator + "cache").mkdir();
    }

    public static void recursiveDeleteFolder(File file) {
        if (!file.exists()) return;
        if (file.isDirectory()) {
            for (File f : file.listFiles()) {
                recursiveDeleteFolder(f);
            }
        }
        file.delete();
    }

    /*public CacheByQueryResponseDto getCacheByQuery(String query) {
        File folder = new File(props.getPROJECT_FOLDER() +
                File.separator + "cache");
        Set<String> gifs = new HashSet<>();
        if (Arrays.asList(folder.list()).contains(query)) {
            folder = new File(props.getPROJECT_FOLDER() +
                    File.separator + "cache" +
                    File.separator + query);
            for (File gif : folder.listFiles()) {
                if (gif.isFile()) gifs.add(gif.toString());
            }
        }
        return new CacheByQueryResponseDto(query, gifs);
    }*/

}
