package com.bsa.spring.hometask;

import com.bsa.spring.hometask.Configuration.Settings;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

import java.io.File;

@SpringBootApplication
@ConfigurationPropertiesScan
public class HometaskApplication {

	public static void main(String[] args) {
		var context = SpringApplication.run(HometaskApplication.class, args);
		Settings settings = context.getBean(Settings.class);
		File folder = new File(settings.getPROJECT_FOLDER() +
				File.separator + "cache");
		if (!folder.exists()) folder.mkdir();
		folder =  new File(settings.getPROJECT_FOLDER() +
				File.separator + "users");
		if (!folder.exists()) folder.mkdir();

	}

}
