package com.bsa.spring.hometask.Filter;

import com.bsa.spring.hometask.Configuration.Settings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class RequestFilter implements Filter {
    @Autowired
    private Settings settings;
    private static final Logger LOGGER = LoggerFactory.getLogger(RequestFilter.class);

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest httpRequest=null;

        httpRequest = (HttpServletRequest) request;
        LOGGER.info("Method: " + httpRequest.getMethod() + ", URL: " + httpRequest.getRequestURL());

        HttpServletResponse myResponse= (HttpServletResponse) response;
        Enumeration<String> headers = httpRequest.getHeaders("X-BSA-GIPHY");
        if (!httpRequest.getRequestURL().toString().contains("/gifs/") &&
                (!httpRequest.getRequestURL().toString().contains("/users/") ||
                !httpRequest.getRequestURL().toString().contains("/cache/"))
                )
        {
            if (!headers.hasMoreElements()) {
                myResponse.sendError(403);
                myResponse.getOutputStream().flush();
                return;
            }
        }
        chain.doFilter(request, response);
    }
}
