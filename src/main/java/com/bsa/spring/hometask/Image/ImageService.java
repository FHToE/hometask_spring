package com.bsa.spring.hometask.Image;

import com.bsa.spring.hometask.Configuration.Settings;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bsa.spring.hometask.Image.Dto.GiphyResponseDto;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

@Service
public class ImageService {
    @Autowired
    private Settings settings;

    public List<String> getImageId (String request) {
        List<String> idSet = new ArrayList<>();
        ObjectMapper mapper = new ObjectMapper();
        // String id = null;
        try {
            GiphyResponseDto obj = mapper.
                    readValue(new URL("http://api.giphy.com/v1/gifs/search?q="
                            + request + "&api_key=" + settings.getAPI_KEY() + "&limit=1000"), GiphyResponseDto.class);
                if (obj.getData().size() == 0) return idSet;
                int rand = (int) (obj.getData().size() * Math.random());
                for (Object ob: obj.getData()) {
                    idSet.add(((LinkedHashMap) ob).get("id").toString());
                }
                // id = (((LinkedHashMap)obj.getData().get(rand)).get("id")).toString();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return idSet;
    }

    public List<File> getAllGifs() {
        List<File> list = new ArrayList<>();
        File folder = new File(settings.getPROJECT_FOLDER() +
                File.separator + "cache");
        for (File subfolder : folder.listFiles()) {
            list.addAll(Arrays.asList(subfolder.listFiles()));
        }
        return list;
    }

    public void saveImage(String imageId, String destinationFile) {
        try {
        URL url = new URL("https://i.giphy.com/media/"+ imageId +"/giphy.gif");
        try (InputStream is = url.openStream(); OutputStream os = new FileOutputStream(destinationFile);) {
            byte[] b = new byte[2048];
            int length;

            while ((length = is.read(b)) != -1) {
                os.write(b, 0, length);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }} catch (MalformedURLException ex) {
            ex.printStackTrace();
        }
        /*URL url = new URL("https://i.giphy.com/media/"+ imageId +"/giphy.webp");
        InputStream is = url.openStream();
        OutputStream os = new FileOutputStream(destinationFile);*/
    }
}
