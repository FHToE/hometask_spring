package com.bsa.spring.hometask.Image;

import com.bsa.spring.hometask.Configuration.Settings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.io.File;
import java.util.List;

@RestController
@RequestMapping("/gifs")
public class ImageController {
    @Autowired
    ImageService imageService;
    @Autowired
    private Settings settings;

    @GetMapping
    public List<File> getImg() {
        return imageService.getAllGifs();
    }

    @GetMapping("/{cacheOrUsername}/{query}/{id}") //Dont work:(
    public ModelAndView getGifByPath(@PathVariable String id, @PathVariable String cacheOrUsername,
                                     @PathVariable String query, ModelAndView modelAndView) {
        String path = settings.getPROJECT_FOLDER();
        if (cacheOrUsername.equals("cache")) {
            path = path + File.separator + cacheOrUsername + File.separator + query + File.separator + id + ".gif";
        } else {
            path = path + File.separator + "users" + File.separator + cacheOrUsername + File.separator + query + File.separator + id + ".gif";
        }
        modelAndView.setViewName("currentgif");
        modelAndView.addObject("path", path);
        return modelAndView;
    }
}
