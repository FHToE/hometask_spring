package com.bsa.spring.hometask.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "gif not found")
public class GifNotFoundException extends RuntimeException{
}
