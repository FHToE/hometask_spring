package com.bsa.spring.hometask.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_ACCEPTABLE, reason = "bad query param")
public class NotValidQueryException extends RuntimeException{
}
