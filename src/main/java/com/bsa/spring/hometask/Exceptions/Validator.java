package com.bsa.spring.hometask.Exceptions;

import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class Validator {
    private static final String FOLDER_PATTERN = "^[A-Za-z_][A-Za-z0-9_]{2,29}$";;
    private final Pattern pattern;
    private Matcher matcher;

    public Validator () {
        pattern = Pattern.compile(FOLDER_PATTERN);
    }

    public boolean validate(String name) {
        matcher = pattern.matcher(name);
        return matcher.matches();
    }

}
