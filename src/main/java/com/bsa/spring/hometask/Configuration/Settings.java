package com.bsa.spring.hometask.Configuration;

import com.bsa.spring.hometask.User.Model.User;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
@Data
public class Settings {
    @Value(value = "${GIPHY_API_KEY}")
    private String API_KEY;
    @Value(value = "${ROOT_FOLDER}")
    private String PROJECT_FOLDER;
    //private Map<String, Map<String, Set<String>>> CACHE = new HashMap<>(); // { user { keyword { gifs }}}

    private List<User> users = new ArrayList<>();

}
