package com.bsa.spring.hometask.Configuration;


import com.bsa.spring.hometask.User.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.io.File;
import java.nio.file.Path;

@Configuration
@EnableWebMvc
public class MvcConfig implements WebMvcConfigurer {
    @Autowired
    private Settings settings;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) { //Dont work:(
        Path path = Path.of(settings.getPROJECT_FOLDER()).toAbsolutePath();
        registry
                .addResourceHandler("/files/**")
                .addResourceLocations("file:///"+path);
    }
}
