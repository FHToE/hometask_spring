package com.bsa.spring.hometask.User.Dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UsersFilesResponseDto {
    private String query;
    private Set<String> gifs;
}
