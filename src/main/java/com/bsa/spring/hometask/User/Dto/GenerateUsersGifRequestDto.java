package com.bsa.spring.hometask.User.Dto;

import lombok.Data;

@Data
public class GenerateUsersGifRequestDto {
    private String query;
    private Boolean force;
}
