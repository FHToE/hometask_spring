package com.bsa.spring.hometask.User;

import ch.qos.logback.core.joran.conditional.IfAction;
import com.bsa.spring.hometask.Image.ImageService;
import com.bsa.spring.hometask.Configuration.Settings;
import com.bsa.spring.hometask.User.Dto.UsersFilesResponseDto;
import com.bsa.spring.hometask.User.Model.Query;
import com.bsa.spring.hometask.User.Model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.time.LocalDate;
import java.util.*;

@Service
public class UserService {
    @Autowired
    private Settings settings;
    @Autowired
    private ImageService imageService;

    public String generateUsersGif(String query, String userId, Boolean force) {
        File cachFile = null;
        File userFile = new File(getUserFolder(userId)
                + File.separator + query);
        if (!userFile.exists()) userFile.mkdir();

        List<String> userGifsList = Arrays.asList(userFile.list());

        File folder = new File(settings.getPROJECT_FOLDER() +
                File.separator + "cache" + File.separator + query);

        if (force || !folder.exists()) { //download gif to cache if folder with this keyword not exists or force = true
            String path = downloadGifToCacheAndGetPath(query, userGifsList);
            if (path==null) {
                userFile.delete();
                return null;
            }
            cachFile = new File(path);
        } else { //look for new gif in cache(that not exists in users folder), if such gif not exists - download new one to cache
            List<File> gifsInCache = new ArrayList<>();
            gifsInCache.addAll(Arrays.asList(folder.listFiles()));
            cachFile = gifsInCache.remove((int)(Math.random() * gifsInCache.size()));
            while (userGifsList.contains(cachFile.getName())) {
                if (gifsInCache.size() == 0) break;
                cachFile = gifsInCache.remove((int)(Math.random() * gifsInCache.size()));
            }
            if (userGifsList.contains(cachFile.getName())) {
                String path = downloadGifToCacheAndGetPath(query, userGifsList);
                if (path==null) {
                    userFile.delete();
                    return null;
                }
                cachFile = new File(path);
            }
        }

        //copy file from cache folder to user's folder

        userFile = new File(userFile.toString() + File.separator + cachFile.getName());
        try (InputStream is = new FileInputStream(cachFile); OutputStream os = new FileOutputStream(userFile);) {
            byte[] buffer = new byte[2048];
            int length;
            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            userFile.delete();
            return null;
        }
        addToCache(userId, query, userFile.toString());
        addToHistory(userId, query, userFile.toString());
        return userFile.toString();
    }

    public List<UsersFilesResponseDto> getUsersGifs (String userId) {
        List<UsersFilesResponseDto> list = new ArrayList<>();
        File folder = new File(settings.getPROJECT_FOLDER() +
                File.separator + "users" + File.separator + userId);
        if (!folder.exists()) return list;
        for (File fold : folder.listFiles()) {
            if (fold.isDirectory()) {
                Set<String> gifs = new HashSet<>();
                for (File gif : fold.listFiles()) {
                    if (gif.isFile()) gifs.add(gif.toString());
                }
                list.add(new UsersFilesResponseDto(fold.getName(), gifs));
            }
        }
        return list;
    }

    public String findGif(String query, Boolean force, String userId) {
        String filepath = null;
        File folder = new File(settings.getPROJECT_FOLDER() +
                File.separator + "users" + File.separator + userId + File.separator + query);
        if (!folder.exists() || folder.list().length == 0) return filepath;
        if (force) {
            filepath = folder.listFiles()[(int)(Math.random() * folder.listFiles().length)].toString();
            addToCache(userId, query, filepath);
            return filepath;
        }
        try {
            List<User> users = settings.getUsers();
            User user = null;
            Optional<User> optuser = users.stream().filter(u -> u.getName().equals(userId)).findFirst();
            if (optuser.isEmpty()) {
                throw new ArrayIndexOutOfBoundsException();
            } else {
                user = optuser.get();
            }
            Optional<Query> optQuery = user.getQueries().stream().filter(q-> q.getQuery().equals(query)).findFirst();
            Query key = null;
            if (optQuery.isEmpty()) {
                throw new ArrayIndexOutOfBoundsException();
            } else {
                key = optQuery.get();
            }
            filepath = key.getGifs().get((int) (Math.random() * key.getGifs().size()));
        } catch (NullPointerException | ArrayIndexOutOfBoundsException ex) {
            filepath = folder.listFiles()[(int)(Math.random() * folder.listFiles().length)].toString();
            addToCache(userId, query, filepath);
        }

        /*try {
           Object[] gifsInCache = settings.getCACHE().get(userId).get(query).toArray();
           if (gifsInCache.length>0) {
               filepath = gifsInCache[(int)(Math.random()*gifsInCache.length)].toString();
           } else {
               throw new ArrayIndexOutOfBoundsException();
           }
        } catch (NullPointerException | ArrayIndexOutOfBoundsException ex) {
            filepath = folder.listFiles()[(int)(Math.random() * folder.listFiles().length)].toString();
            addToCache(userId, query, filepath);
        }*/
        return filepath;
    }

    public void deleteCacheByQuery(String userId, String keyWord) {
        // System.out.println(settings.getCACHE());
        List<User> users = settings.getUsers();
        System.out.println(users);
        User user = null;
        Optional<User> optuser = users.stream().filter(u -> u.getName().equals(userId)).findFirst();
        if (optuser.isEmpty()) {
            return;
        } else {
            user = optuser.get();
        }
        Optional<Query> optQuery = user.getQueries().stream().filter(q-> q.getQuery().equals(keyWord)).findFirst();
        Query query = null;
        if (optQuery.isEmpty()) {
            return;
        } else {
            query = optQuery.get();
        }
        user.getQueries().remove(query);

        /*if (!settings.getCACHE().containsKey(userId)) return;
        if (query == null) {
            settings.getCACHE().remove(userId);
            return;
        }
        Map<String, Set<String>> userCache = settings.getCACHE().get(userId);
        if (userCache.containsKey(query)) userCache.remove(query);*/ // variant with Map
    }

    public void hardDeleteUsersData(String userId) {
        deleteCacheByQuery(userId, null);
        recursiveDeleteFolder(new File(settings.getPROJECT_FOLDER() +
                File.separator + "users" + File.separator + userId));
    }

    public static void recursiveDeleteFolder(File file) {
        if (!file.exists()) return;
        if (file.isDirectory()) {
            for (File f : file.listFiles()) {
                recursiveDeleteFolder(f);
            }
        }
        file.delete();
    }

    public List<Map<String,String>> getUsersHistory(String userId) {
        List<Map<String,String>> list = new ArrayList<>();
        File history = new File(settings.getPROJECT_FOLDER() +
                File.separator + "users" + File.separator + userId + File.separator + "history.csv");
        if (!history.exists()) return list;
        try (BufferedReader reader = new BufferedReader(new FileReader(history))) {
            String line = reader.readLine();
            while (line!=null) {
                String[] props = line.split(", ");
                Map<String,String> map = new HashMap<>();
                map.put("date", props[0]);
                map.put("query", props[1]);
                map.put("gif", props[2]);
                list.add(map);
                line = reader.readLine();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return list;
    }

    public void deleteHistory(String userId) {
        File history = new File(settings.getPROJECT_FOLDER() +
                File.separator + "users" + File.separator + userId + File.separator + "history.csv");
        if (!history.exists()) return;
        try (FileWriter writer = new FileWriter(history)) {
            writer.write("");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void addToCache (String userId, String gifKeyWord, String path) {
        List<User> users = settings.getUsers();
        User user = null;
        Optional<User> optuser = users.stream().filter(u -> u.getName().equals(userId)).findFirst();
        if (optuser.isEmpty()) {
            user = new User(userId, new ArrayList<Query>());
            users.add(user);
        } else {
            user = optuser.get();
        }
        Optional<Query> optQuery = user.getQueries().stream().filter(q-> q.getQuery().equals(gifKeyWord)).findFirst();
        Query query = null;
        if (optQuery.isEmpty()) {
            query = new Query(gifKeyWord, new ArrayList<String>());
            user.getQueries().add(query);
        } else {
            query = optQuery.get();
        }
        query.getGifs().add(path);

        /* variant with Map
        Map<String, Map<String, Set<String>>> cache = settings.getCACHE();
        if (!cache.containsKey(userId)) cache.put(userId, new HashMap<String, Set<String>>());
        Map<String, Set<String>> usersCache = cache.get(userId);
        if (!usersCache.containsKey(gifKeyWord)) usersCache.put(gifKeyWord, new HashSet<String>());
        usersCache.get(gifKeyWord).add(path);*/
    }

    public void addToHistory(String userId, String gifKeyWord, String path) {
        File history = new File(getUserFolder(userId) + File.separator + "history.csv");
        try (FileWriter writer = new FileWriter(history, true)) {
            writer.write(LocalDate.now() + ", " + gifKeyWord + ", " + path + "\n");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public String downloadGifToCacheAndGetPath (String query, List<String> userGifsList) {
        File folder = new File(settings.getPROJECT_FOLDER() +
                File.separator + "cache" + File.separator + query);

        if (!folder.exists()) folder.mkdir();
        List<String> idList = imageService.getImageId(query);
        if (idList.size() == 0) {
            folder.delete();
            return null;
        }
        String gifId = idList.remove((int) (Math.random() * idList.size()));
        String filename = gifId + ".gif";
        while  (Arrays.asList(folder.list()).contains(filename) || userGifsList.contains(filename)) {
            if (idList.size() == 0) {
                folder.delete();
                return null;
            }
            gifId = idList.remove((int) (Math.random() * idList.size()));
            filename = gifId + ".gif";
        }
        String filepath = folder + File.separator + filename;
        imageService.saveImage(gifId, filepath);
        return filepath;
    }

    public String getUserFolder(String id) {
        File folder = new File(settings.getPROJECT_FOLDER() +
                File.separator + "users" + File.separator + id);
        if (!folder.exists()) {
            folder.mkdir();
            try {
                new File(folder.toString() + File.separator + "history.csv").createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return folder.toString();
    }
}
