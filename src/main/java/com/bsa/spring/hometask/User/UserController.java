package com.bsa.spring.hometask.User;

import com.bsa.spring.hometask.Exceptions.NotValidQueryException;
import com.bsa.spring.hometask.Exceptions.Validator;
import com.bsa.spring.hometask.User.Dto.GenerateUsersGifRequestDto;
import com.bsa.spring.hometask.User.Dto.UsersFilesResponseDto;
import com.bsa.spring.hometask.Exceptions.GifNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private Validator validator;

    @PostMapping("/{id}/generate")
    public String generateUsersGif (@RequestBody GenerateUsersGifRequestDto request,
                                    @PathVariable String id) {
        if (!validator.validate(id)) throw new NotValidQueryException();
        String result = userService.generateUsersGif(request.getQuery(), id, request.getForce());
        if (result==null) throw new GifNotFoundException();
        return result;
    }

    @GetMapping("/{id}/all")
    public List<UsersFilesResponseDto> getUsersGifs(@PathVariable String id) {
        return userService.getUsersGifs(id);
    }

    @GetMapping("/{id}/history")
    public List<Map<String,String>> getUsersHistory(@PathVariable String id) {
        return userService.getUsersHistory(id);
    }

    @GetMapping("/{id}/search")
    public String findGif (@RequestParam String query,
                           @RequestParam(defaultValue = "false") Boolean force,
                           @PathVariable String id)
        {
            String result = userService.findGif(query, force, id);
            if (result == null) throw new GifNotFoundException();
            return result;
        }

    @DeleteMapping("/{id}/history/clean")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteHistory(@PathVariable String id) {
        userService.deleteHistory(id);
    }

    @DeleteMapping("/{id}/reset")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteCacheByQuery (@RequestParam(required = false) String query, @PathVariable String id) {
        userService.deleteCacheByQuery(id, query);
    }

    @DeleteMapping("/{id}/clean")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void hardDeleteUsersData(@PathVariable String id) {
        userService.hardDeleteUsersData(id);
    }

}
